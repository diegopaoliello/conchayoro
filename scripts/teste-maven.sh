#!/bin/bash

ARTIFACT_REPOSITORY_ENABLED="False"
QUALITY_REPOSITORY_ENABLED="False"

SETTINGS="-settings .docker/maven/settings.xml"

if [ ARTIFACT_REPOSITORY_ENABLED == "True" ]
then
   SETTINGS="-settings .docker/maven/settings-com-nexus.xml"}
fi


echo 'Preparando containeres ...'

echo 'Executando ciclo clean, compile, package, install, deploy...'

echo 'Executando clean...'

docker-compose run --rm maven mvn clean $SETTINGS
	
echo 'Executando compile...'

docker-compose run --rm maven mvn compile $SETTINGS

echo 'Executando package...'

docker-compose run --rm maven mvn package $SETTINGS

echo 'Executando install...'

docker-compose run --rm maven mvn install $SETTINGS

if [ ARTIFACT_REPOSITORY_ENABLED == "True" ]
then {
    echo 'Executando deploy Nexus...'
    docker-compose run --rm maven mvn deploy $SETTINGS
}
fi

echo 'Executando deploy Wildfly...'

docker-compose run --rm maven mvn wildfly:deploy $SETTINGS

echo 'Executando teste de integração...'

docker-compose run --rm maven mvn failsafe:integration-test $SETTINGS

if [ QUALITY_REPOSITORY_ENABLED == "True" ]
then {
  echo 'Publicando no Sonar...'
  docker-compose run --rm maven mvn sonar:sonar -Dsonar.host.url=http://sonar:9000 $SETTINGS
}
fi





